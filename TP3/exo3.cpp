#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

struct SearchTreeNode : public Node
{    
    SearchTreeNode* left;
    SearchTreeNode* right;
    int value;

    void initNode(int value)
    {
        this->value=value;
        this->left=0;
        this->right=0;
    }

    void insertNumber(int value)
    {
        SearchTreeNode* newNode = new SearchTreeNode(value);

        if(value<=this->value)
        {
            if (this->left==nullptr)
            {
                this->left=newNode;
                return;
            }
            else
            {
                this->left->insertNumber(value);
            }
        }
        else
        {
            if (this->right==nullptr)
            {
                this->right=newNode;
                return;
            }
            else
            {
                this->right->insertNumber(value);
            }
        }
    }

	uint height() const	{
        int heightLeft, heightRight;

        if (this->left==nullptr && this->right==nullptr)
        {
            return 1;
        }

        else
        {
            if (this->left==nullptr)
            {
                heightLeft = 0;
            }
            else
            {
                heightLeft = this->left->height();
            }

            if (this->right==nullptr)
            {
                heightRight = 0;
            }
            else
            {
                heightRight = this->right->height();
            }

            if (heightLeft>=heightRight)
            {
                return 1+heightLeft;
            }
            else
            {
                return 1+heightRight;
            }
        }
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1

    }

	uint nodesCount() const {
        int nodeLeft, nodeRight;

        if (this->left==nullptr && this->right==nullptr)
        {
            return 1;
        }

        else
        {
            if (this->left==nullptr)
            {
                nodeLeft = 0;
            }
            else
            {
                nodeLeft = this->left->nodesCount();
            }

            if (this->right==nullptr)
            {
                nodeRight = 0;
            }
            else
            {
                nodeRight = this->right->nodesCount();
            }

            return nodeRight+nodeLeft+1;
        }
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1
	}

	bool isLeaf() const {
        if (this->left==nullptr && this->right==nullptr)
        {
            return true;
        }
        return false;

        // return True if the node is a leaf (it has no children)
	}

	void allLeaves(Node* leaves[], uint& leavesCount) {
        // fill leaves array with all leaves of this tree
	}

	void inorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travel
	}

	void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel
	}

	void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel
	}

	Node* find(int value) {
        // find the node containing value
		return nullptr;
	}

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    SearchTreeNode(int value) : Node(value) {initNode(value);}
    ~SearchTreeNode() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new SearchTreeNode(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<SearchTreeNode>();
	w->show();

	return a.exec();
}
