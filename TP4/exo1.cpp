#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
    return nodeIndex*2+1;
}

int Heap::rightChild(int nodeIndex)
{
    return nodeIndex*2+2;
}

void Heap::insertHeapNode(int heapSize, int value)
{
    int i=heapSize;
    this->get(i)=value;
    while (i>0 && this->get(i) > this->get((i-1)/2))
    {
        int Temp = (*this)[i];
        (*this)[i] = (*this)[(i-1)/2];
        (*this)[(i-1)/2]=Temp;
        i=(i-1)/2;
    }
	// use (*this)[i] or this->get(i) to get a value at index i
}

void Heap::heapify(int heapSize, int nodeIndex)
{
    int i_max = nodeIndex;

    if (leftChild(nodeIndex) < heapSize && (*this)[i_max]< (*this)[leftChild(nodeIndex)]) //si index de gauche est inférieur à la taille
    {
        i_max=leftChild(nodeIndex);
    }

    if (rightChild(nodeIndex) < heapSize && (*this)[i_max]<(*this)[rightChild(nodeIndex)])
    {
        i_max=rightChild(nodeIndex);
    }

    if (i_max!=nodeIndex)
    {
        swap(nodeIndex,i_max);
        heapify(heapSize,i_max);
    }
    // use (*this)[i] or this->get(i) to get a value at index i
}

void Heap::buildHeap(Array& numbers)
{
    for (int i=0;i<numbers.size();i++)
    {
        insertHeapNode(i, numbers[i]);
    }
}

void Heap::heapSort()
{
    for (int i=this->size()-1;i>0;i--)
    {
        swap(0,i);
        heapify(i,0);
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
