#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    // your code
};

struct DynaTableau{
    int* donnees;
    int index;
    int capacite;
};


void initialise(Liste* liste)
{
    liste->premier = nullptr;
}

bool est_vide(const Liste* liste)
{
    if (liste->premier == nullptr)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void ajoute(Liste* liste, int valeur)
{
    Noeud* nouveauNoeud = new Noeud;
    nouveauNoeud->donnee = valeur;
    nouveauNoeud->suivant = nullptr;

    if (liste->premier == nullptr)
    {
        liste->premier = nouveauNoeud;
    }
    else
    {
        Noeud* dernier = liste->premier;
        while (dernier->suivant != nullptr)
        {
            dernier = dernier->suivant;
        }
        dernier->suivant = nouveauNoeud;
    }
}

void affiche(const Liste* liste)
{
    if (liste->premier != nullptr)
    {
        Noeud* noeud = liste->premier;
        while (noeud != nullptr)
        {
            std::cout << noeud->donnee << std::endl;
            noeud = noeud->suivant;
        }
    }
}

int recupere(const Liste* liste, int n)
{
    Noeud* noeud = liste->premier;
    int i = 0;
    while (i < n)
    {
        noeud = noeud->suivant;
        i++;
    }
    return noeud->donnee;
}

int cherche(const Liste* liste, int valeur)
{
    Noeud* noeud = liste->premier;
    int i = 0;
    while (noeud->donnee != valeur)
    {
        noeud = noeud->suivant;
        i++;
    }
    return i + 1;
}

void stocke(Liste* liste, int n, int valeur)
{
    Noeud* noeud = liste->premier;
    int i = 0;
    while (i < n)
    {
        noeud = noeud->suivant;
        i++;
    }
    noeud->donnee = valeur;
}

///////////////////////////////////MA VERSION
void initialise(DynaTableau* tableau, int capacite)
{
    tableau->donnees=new int[capacite];
    tableau->index=0;
    tableau->capacite=capacite;
}

void ajoute(DynaTableau* tableau, int valeur)
{
    if (tableau->index+1>tableau->capacite)
    {
        tableau->capacite+=5;
        int* newTab = new int[tableau->capacite];

        for (int i = 0; i<tableau->index; i++)
        {
            newTab[i]=tableau->donnees[i];
        }
        newTab[tableau->index]=valeur;
        tableau->index++;

        tableau->donnees = newTab;
    }

    else
    {
        tableau->donnees[tableau->index]=valeur;
        tableau->index++;
    }
}

bool est_vide(const DynaTableau* tableau)
{
    if (tableau->index==0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void affiche(const DynaTableau* tableau)
{
    for (int i = 0; i<tableau->index;i++)
    {
        std::cout << "Case " << i << " : " << tableau->donnees[i] << endl;
    }
}

int recupere(const DynaTableau* tableau, int n)
{
    if (n<=tableau->capacite)
    {
        return tableau->donnees[n];
    }
    else
    {
        return 0;
    }
}

int cherche(const DynaTableau* tableau, int valeur)
{
    for (int i = 0; i < tableau->capacite; i++)
    {
        if (tableau->donnees[i]==valeur)
        {
            return i;
        }
    }
    return -1;
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    if (n<=tableau->capacite)
    {
        tableau->donnees[n]=valeur;
    }
    else
    {
        DynaTableau* tableauTemp = new DynaTableau;
        initialise(tableauTemp,n);

        for (int i = 0; i<tableau->capacite;i++)
        {
            tableauTemp->donnees[i]=tableau->donnees[i];
        }
        tableau = tableauTemp;
        tableau->donnees[n]=valeur;
        std::cout << "capacite : " << tableau->capacite;
        delete tableauTemp;
    }

}

void pousse_pile(Liste* liste, int valeur)
{
    Noeud *nouveau=new Noeud;
    nouveau->donnee=valeur;
    nouveau->suivant=liste->premier;
    liste->premier=nouveau;
}

int retire_pile(Liste* liste)
{
    int temp = liste->premier->donnee;
    liste->premier=liste->premier->suivant;
    return temp;
}

void pousse_file(DynaTableau* tableau, int valeur)
{
    tableau->donnees[tableau->index]=valeur;
    tableau->index++;
}

int retire_file(DynaTableau* tableau)
{
    int Temp = tableau->donnees[0];
    for (int i=0;i<tableau->index;i++)
    {
        tableau->donnees[i]=tableau->donnees[i+1];
    }
    tableau->index--;
    return Temp;
}
///////////////////////////////////

int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5); //

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))//
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);//
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    //
    cout << "main : tableau.capacite : " << tableau.capacite << endl;
    cout << "main : tableau.index : " << tableau.index << endl;
    /*for (int i = 0; i<tableau.index;i++)
    {
        std::cout << "Case " << i << " : " << tableau.donnees[i] << endl;
    }*/
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    DynaTableau file;
    Liste pile; // DynaTableau file;

    initialise(&file, 5);
    initialise(&pile);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;

    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
