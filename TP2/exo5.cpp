#include <QApplication>
#include <time.h>
#include "tp2.h"

MainWindow* w=nullptr;

void merge(Array& first, Array& second, Array& result);

void splitAndMerge(Array& origin)
{
    if (origin.size()<=1)
    {
        return;
    }
	// stop statement = condition + return (return stop the function even if it does not return anything)

	// initialisation
	Array& first = w->newArray(origin.size()/2);
	Array& second = w->newArray(origin.size()-first.size());
	
	// split
    qDebug ("?");
    for (int i=0;i<origin.size()/2;i++)
    {
        first[i]=origin[i];
        second[i]=origin[origin.size()/2+i];
    }

    qDebug ("??");
	// recursiv splitAndMerge of lowerArray and greaterArray
    splitAndMerge(first);
    splitAndMerge(second);

	// merge
    merge(first,second,origin);
}

void merge(Array& first, Array& second, Array& result)
{
    qDebug ("???");
    int indexFirst = 0;
    int indexSecond = 0;
    int i = 0;

    while (indexFirst <= first.size() && indexSecond <= second.size())
    {
        qDebug ("????");

        if(second[indexSecond]<first[indexFirst])
        {
            result[i++]=second[indexSecond++];
        }

        else
        {
            result[i++]=first[indexFirst++];
        }


        qDebug ("?????");

        if (indexFirst < first.size())
        {
            for (int j=indexFirst; j<first.size();j++)
            {
                result[i++]=first[j];
            }
        }


        else if (indexSecond < second.size())
        {
            for (int j=indexSecond; j<second.size();j++)
            {
                result[i++]=second[j];
            }
        }
    }
}

void mergeSort(Array& toSort)
{
    splitAndMerge(toSort);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(mergeSort);
	w->show();

	return a.exec();
}
