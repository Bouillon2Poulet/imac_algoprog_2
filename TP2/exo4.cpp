#include <QApplication>
#include <time.h>
#include <iostream>

#include "tp2.h"

using namespace std;

MainWindow* w=nullptr;

void recursivQuickSort(Array& toSort, int size)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)
    int pivot = toSort[size/2];
    qDebug ("pivot : %d",pivot);
	Array& lowerArray = w->newArray(size);
	Array& greaterArray= w->newArray(size);
	int lowerSize = 0, greaterSize = 0; // effectives sizes
    if (size==1||size==0)
    {
        return;
    }
	// split
    for (int i=0;i<size;i++)
    {
        if (toSort[i]>pivot)
        {
            greaterArray[greaterSize]=toSort[i];
            greaterSize++;
        }

        else if (i==size/2)
        {

        }

        else
        {
            lowerArray[lowerSize]=toSort[i];
            lowerSize++;
        }
    }

    // recursiv sort of lowerArray and greaterArray
    recursivQuickSort(lowerArray,lowerSize);
    recursivQuickSort(greaterArray,greaterSize);

	// merge
    for (int j=0;j<size/2;j++)
    {
        toSort[j]=lowerArray[j];
    }

    toSort[lowerSize]=pivot;

    for (int j=lowerSize+1;j<size;j++)
    {
        toSort[j]=greaterArray[j-lowerSize-1];
    }
}

void quickSort(Array& toSort){
	recursivQuickSort(toSort, toSort.size());
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(quickSort);
	w->show();

	return a.exec();
}
